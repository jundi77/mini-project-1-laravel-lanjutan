<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'nomor_HP' => '0',
            'password' => bcrypt('secret'),
        ]);
        $user->roles()->attach($user->compactRolePermission('admin')['role']);
        // $user->permissions()->attach($user->compactRolePermission('admin')['permission']);

        $user = User::create([
            'nomor_HP' => '1',
            'password' => bcrypt('secret'),
        ]);
        $user->roles()->attach($user->compactRolePermission('pelayanan')['role']);
        // $user->permissions()->attach($user->compactRolePermission('pelayanan')['permission']);

        $user = User::create([
            'nomor_HP' => '2',
            'password' => bcrypt('secret'),
        ]);
        $user->roles()->attach($user->compactRolePermission('kurir')['role']);
        // $user->permissions()->attach($user->compactRolePermission('kurir')['permission']);
    }
}
