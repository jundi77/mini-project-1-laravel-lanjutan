<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
            'name' => 'Admin',
            'slug' => 'admin'
        ]);
        $role->permissions()->attach($role->compactRolePermission('admin')['permission']);
        
        $role = Role::create([
            'name' => 'Pos Pelayanan',
            'slug' => 'pelayanan'
        ]);
        $role->permissions()->attach($role->compactRolePermission('pelayanan')['permission']);

        $role = Role::create([
            'name' => 'Pos Kurir',
            'slug' => 'kurir'
        ]);
        $role->permissions()->attach($role->compactRolePermission('kurir')['permission']);
        
        // $pos_pelayanan = new Role();
        // $pos_pelayanan->name = 'Pos Pelayanan';
        // $pos_pelayanan->slug = 'pelayanan';
        // $pos_pelayanan->save();

        // $pos_kurir = new Role();
        // $pos_kurir->name = 'Pos Kurir';
        // $pos_kurir->slug = 'kurir';
        // $pos_kurir->save();
    }
}
