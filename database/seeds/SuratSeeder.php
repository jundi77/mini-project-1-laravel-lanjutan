<?php

use Illuminate\Database\Seeder;
use App\Surat;

class SuratSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Surat::create([
            'kodepos' => '10',
            'alamat-pengirim' => 'Namek',
            'alamat-tertuju' => 'Mars',
        ]);

        Surat::create([
            'kodepos' => '01',
            'alamat-pengirim' => 'Namek',
            'alamat-tertuju' => 'Mars',
            'alamat-pengganti' => 'Bumi'
        ]);
        
    }
}
