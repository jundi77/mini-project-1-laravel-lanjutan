<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tambah_surat = new Permission();
        $tambah_surat->name = 'Menambah surat';
        $tambah_surat->slug = 'tambah-surat';
        $tambah_surat->save();

        $ubah_ditahan = new Permission();
        $ubah_ditahan->name = 'Mengubah status ditahan';
        $ubah_ditahan->slug = 'ubah-ditahan';
        $ubah_ditahan->save();

        $ubah_alamat_pengganti = new Permission();
        $ubah_alamat_pengganti->name = 'Mengubah alamat pengganti';
        $ubah_alamat_pengganti->slug = 'ubah-alamat-pengganti';
        $ubah_alamat_pengganti->save();

        $ubah_lain2_surat = new Permission();
        $ubah_lain2_surat->name = 'Ubah lain2 surat';
        $ubah_lain2_surat->slug = 'ubah-lain2-surat';
        $ubah_lain2_surat->save();

        $ganti_role = new Permission();
        $ganti_role->name = 'Mengubah role user';
        $ganti_role->slug = 'ubah-role';
        $ganti_role->save();

        $tambah_permission = new Permission();
        $tambah_permission->name = 'Menambah permission user';
        $tambah_permission->slug = 'tambah-permission';
        $tambah_permission->save();
    }
}
