<?php

use App\Http\Controllers\Surat\SuratController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Route::namespace('Auth')->group(function() {
	Route::post('register', 'RegisterController');
	Route::post('login', 'LoginController');
	Route::post('logout', 'LogoutController');
});

Route::group(['middleware' => ['auth:api', 'role:admin,pelayanan,kurir']], function() {
	Route::put('surat/edit-kurir', function(Request $request) {
		$request->only(['kodepos', 'ditahan', 'alamat-pengganti']);
		$controller = new SuratController();
		return $controller->update($request);
	});
	Route::post('alamat-kodepos', 'Surat\SuratController@getAlamatKodepos');
	Route::post('surat-di-kodepos', 'Surat\SuratController@show');
});

Route::group(['middleware' => ['auth:api', 'role:admin,pelayanan']], function() {
	Route::post('surat/tambah', 'Surat\SuratController@show');
	Route::put('surat/edit', 'Surat\SuratController@update');
});

Route::group(['middleware' => ['auth:api', 'role:admin']], function() {
	Route::delete('surat/hapus', 'Surat\SuratController@destroy');
});