<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    protected $fillable = [
        'kodepos', 'belum-terkirim', 'alamat-pengirim',
        'alamat-tertuju', 'alamat-pengganti', 'ditahan'
    ];
}
