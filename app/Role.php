<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RolePermissionVar;

class Role extends Model
{
    use RolePermissionVar;

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'roles_permissions');
    }
}
