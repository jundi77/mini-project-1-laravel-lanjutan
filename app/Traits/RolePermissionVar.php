<?php

namespace App\Traits;

use App\Role;
use App\Permission;

trait RolePermissionVar
{
	public function compactRolePermission(string $slug)
	{

		if ($slug == 'admin')
		return [
			"role" => Role::where('slug', 'admin')->first()->id,
			"permission" => [
				Permission::where('slug', 'tambah-surat')->first()->id,
				Permission::where('slug', 'ubah-ditahan')->first()->id,
				Permission::where('slug', 'ubah-alamat-pengganti')->first()->id,
				Permission::where('slug', 'ubah-lain2-surat')->first()->id,
				Permission::where('slug', 'ubah-role')->first()->id,
				Permission::where('slug', 'tambah-permission')->first()->id
			]
		];
		if ($slug == 'pelayanan')
		return [
			"role" => Role::where('slug', 'pelayanan')->first()->id,
			"permission" => [
				Permission::where('slug', 'tambah-surat')->first()->id,
				Permission::where('slug', 'ubah-ditahan')->first()->id,
				Permission::where('slug', 'ubah-alamat-pengganti')->first()->id,
				Permission::where('slug', 'ubah-lain2-surat')->first()->id,
			]
		];
		if ($slug == 'kurir')
		return [
			"role" => Role::where('slug', 'kurir')->first()->id,
			"permission" => [
				Permission::where('slug', 'ubah-ditahan')->first()->id,
				Permission::where('slug', 'ubah-alamat-pengganti')->first()->id,
				Permission::where('slug', 'ubah-lain2-surat')->first()->id,
			]
		];
	}

}