<?php
namespace App\Traits;

use App\Role;
use App\Permission;
use Mockery\Generator\StringManipulation\Pass\Pass;

trait HasRolesAndPermissions
{
	// user can have many roles
	public function roles()
	{
		return $this->belongsToMany(Role::class, 'users_roles');
	}

	// user can have many permissions
	public function permissions()
	{
		return $this->belongsToMany(Permission::class, 'users_permissions');
	}

	// check of user has roles
	public function hasRole(... $roles)
	{
		foreach ($roles as $role) {
			if ($this->roles->contains('slug', $role)) {
				return true;
			}
		}

		return false;
	}

	// check if user has permissions (from db)
	protected function hasPermission($permission)
	{
		return (bool) $this->permissions->where('slug', $permission->slug)->count();
	}

	// check if user has permissions (from role)
	public function hasPermissionThroughRole($permission)
	{
		foreach ($permission->roles as $role) {
			if($this->roles->contains($role)) {
				return true;
			}
		}

		return false;
	}

	// check if user has permission directly or through role
	protected function hasPermissionTo($permission)
	{
		return $this->hasPermissionThroughRole($permission) || $this->hasPermission($permission);
	}

	// get all permission from given slug
	protected function getAllPermissions(array $permissions) {
		return Permission::whereIn('slug', $permissions)->get();
	}

	// set permission to user (without role)
	public function givePermissionTo(... $permissions) {
		$permissions = $this->getAllPermissions($permissions);
		if ($permissions == null) {
			return $this;
		}
		$this->permission()->saveMany($permissions);
		return $this;
	}

	// delete permission from user
	public function deletePermissions(... $permissions)
	{
		$permissions = $this->getAllPermissions($permissions);
		$this->permissions()->detach($permissions);
		return $this;
	}

	// reassign permission with other array of permissions
	public function refreshPermissions(... $permissions)
	{
		$this->permissions()->detach();
		return $this->givePermissionTo($permissions);
	}

	public function getAllRoles(array $roles)
	{
		return Role::whereIn('slug', $roles)->get();
	}

	public function changeRole(... $roles)
	{
		$roles = $this->getAllRoles($roles);
		if ($roles == null) {
			return $this;
		}
		$this->roles()->detach();
		$this->roles()->saveMany($roles);
		return $this;
	}
}