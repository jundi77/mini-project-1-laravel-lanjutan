<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'nomor_HP' => ['required', 'digits_between:10,20', 'unique:users,nomor_HP'],
            'password' => ['required', 'min:6'],
            'pekerjaan' => ['required']
        ]);

        if (request('pekerjaan') == 'Pos Pelayanan') {
            $new_user = User::create([
                'nomor_HP' => request('nomor_HP'),
                'password' => bcrypt(request('password'))
            ]);
            $new_user->roles()->attach($new_user->compactRolePermission('pelayanan')['role']);
        } else if (request('pekerjaan') == 'Pos Kurir') {
            $new_user = User::create([
                'nomor_HP' => request('nomor_HP'),
                'password' => bcrypt(request('password'))
            ]);
            $new_user->roles()->attach($new_user->compactRolePermission('kurir')['role']);
        }
        else {
            return response()->json([
                "status" => "Pekerjaan tidak dikenal"
            ]);
        }
        return response()->json([
            "status" => "Registrasi akun sukses."
        ]);
    }
}
