<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'nomor_HP' => ['required'],
            'password' => ['required']
        ]);

        if (!$token = auth()->attempt($request->only('nomor_HP', 'password'))) {
            return response(401)->json([
                'status' => "Login gagal"
            ]);
        }

        return response()->json([
            'status' => 'Login berhasil',
            'token' => $token
        ]);
    }
}
