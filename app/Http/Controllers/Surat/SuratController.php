<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Surat;

class SuratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kodepos' => ['required', 'digits_between:4,7'],
            'alamat-pengirim' => 'required',
            'alamat-tertuju' => 'required',
        ]);

        $surat = Surat::create([
            'kodepos' => request('kodepos'),
            'alamat-pengirim' => request('alamat-pengirim'),
            'alamat-tertuju' => request('alamat-tertuju'),
        ]);

        return $surat;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $request->validate([
            'kodepos' => ['required', 'digits_between:4,7']
        ]);
        $belum_terkirim = Surat::select('alamat-pengirim',
                                        'alamat-tertuju',
                                        'alamat-pengganti',
                                        'ditahan')
                            ->where('kodepos', request('kodepos'))
                            ->where('belum-terkirim', 1)->get();
        $sudah_terkirim = Surat::select('alamat-pengirim',
                                        'alamat-tertuju',
                                        'alamat-pengganti',)
                            ->where('kodepos', request('kodepos'))
                            ->where('belum-terkirim', 0)->get();

        return response()->json([
            'kodepos' => request('kodepos'),
            'surat' => [
                'belum-terkirim' => [
                    'jumlah' => $belum_terkirim->count(),
                    'detail-surat' => $belum_terkirim
                ],
                'sudah-terkirim' => [
                    'jumlah' => $sudah_terkirim->count(),
                    'detail-surat' => $sudah_terkirim
                ]
            ]
        ]);
    }

    public function getAlamatKodepos(Request $request)
    {
        $request->validate([
            'kodepos' => ['required', 'digits_between:4,7']
        ]);

        $alamat = Surat::select('alamat-tertuju')
                    ->where('alamat-pengganti', null)
                    ->get();
        $alamat->merge(
            Surat::select('alamat-pengganti')->get()
        );

        return response()->json([
            'kodepos' => request('kodepos'),
            'alamat' => $alamat
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'id_surat' => 'required'
        ]);
        $data = Surat::find(request('id_surat'));
        $data->update([
            'kodepos' => request('kodepos') ?? $data->kodepos,
            'belum-terkirim' => request('belum-terkirim') ?? $data->get('belum-terkirim'),
            'alamat-pengirim' => request('alamat-pengirim') ?? $data->get('alamat-pengirim'),
            'alamat-tertuju' => request('alamat-tertuju') ?? $data->get('alamat-tertuju'),
            'alamat-pengganti' => request('alamat-pengganti') ?? $data->get('alamat-pengganti'),
            'ditahan' => request('ditahan') ?? $data->get('ditahan')
        ]);
        return response()->json([
            'status' => 'Update sukses',
            compact('data')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'id_surat' => ['required']
        ]);
        
        if(!$surat = Surat::find(request('id_surat'))) {
            return response()->json([
                'status' => 'Surat tidak ditemukan'
            ]);
        }

        $surat->delete();

        return response()->json([
            'status' => 'Surat berhasil dihapus'
        ]);
    }
}
